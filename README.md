### Prerequisites

- Python 3

### Installation

After cloning the repository, change to the cloned directory and install the requirements with the following command.

```
pip install -r requirements.txt
```


### Usage

To start the Flask application, start the application by simply calling the app.py file from the root of the cloned repository

```bash
python3 app.py
```

### License

MIT

### Docker Instructions

This is a working flask application that uses Python. Your task is to containerize this application and navigate to it
on your local browser.